
public class PixelCount {
	private int decValue;
	private int frequency;
	
	/**
	 * decValue is the decimal representation
	 * of the pixel
	 * @param decValue
	 */
	public PixelCount (int decValue) {
		this.decValue = decValue;
	}
	
	/**
	 * Increments the frequency/occurrence
	 * value of this pixel
	 */
	public void incrementFreq () {
		frequency++;
	}
	
	/**
	 * Retrieves the RGB decimal value
	 * @return RGB decimal value of the pixel
	 */
	public int getDecValue () {
		return this.decValue;
	}
	
	/**
	 * Retrieves frequency of specific hex value
	 * @return frequency/occurrence of pixel
	 */
	public int getFrequency () {
		return this.frequency;
	}
}
