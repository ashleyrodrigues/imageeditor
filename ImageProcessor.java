import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.HashSet;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Scanner;

import javax.imageio.ImageIO;


public class ImageProcessor {
	private BufferedImage img;
	
	private File fImage; 
	private String imgType;
	private HashMap<Integer, PixelCount> colPresent;
	private HashMap<Integer, Integer> changeTo;
	private Queue<PixelCount> freqPixel;
	private int maxCount;
	private int maxCol;
	private int amntPixels;
	private static Scanner s;
	/**
	 * Checks if the file path exists and tries to
	 * obtain an image, which I/O actions
	 * can be performed
	 * @param fImage location of image file
	 */	
	public ImageProcessor (File fImage) {
		try {
			this.img = ImageIO.read(fImage);
			this.imgType = fImage.getName().split("\\.")[1];
			this.fImage = fImage;
			this.amntPixels = img.getHeight()*img.getWidth();
			this.changeTo = new HashMap<Integer, Integer>();
			this.colPresent = new HashMap<Integer, PixelCount>();
			this.freqPixel = new PriorityQueue<PixelCount>(amntPixels/3,
												new PixelCountComparator());
		} catch (Exception e) {
			System.out.println("Failed to read the image");
			e.printStackTrace();
		}
		
	}
	
	/**
	 * Counts the occurrence of each pixel in the image
	 * using a hashmap and stores the most recurring pixel 
	 * as a global variable
	 */
	public void countPixels () {
		for (int i = 0; i < img.getHeight(); i++) {
			for (int z = 0; z < img.getWidth(); z++) {
				int k = 1;
				int pixelRGB = img.getRGB(z, i);
				try {
					PixelCount pc = colPresent.get(pixelRGB);
					pc.incrementFreq();						
				} catch (NullPointerException e) {
					PixelCount pc = new PixelCount(pixelRGB);
					pc.incrementFreq();
					colPresent.put(pixelRGB, pc);
					freqPixel.add(pc);
				}
			}
		}
		maxCol = freqPixel.peek().getDecValue();
		maxCount = freqPixel.peek().getFrequency();
		System.out.println("Most popular color is: " + maxCol
				+ " with frequency " + maxCount + ". " + img.getHeight()*img.getWidth()
				+ " pixels were analysed");
	}
	
	/**
	 * Replace the RGB value of all pixels with a specific 
	 * RGB value to another RGB value
	 * @param targetColor the color to change
	 * @param changeTo the color to change to
	 */
	public void changeColor (int targetColor, int changeTo) {
		if (targetColor == -1) targetColor = freqPixel.poll().getDecValue();
		for (int i = 0; i < img.getHeight(); i++) {
			for (int z = 0; z < img.getWidth(); z++) {
				if (img.getRGB(z, i) == targetColor) {
					img.setRGB(z, i, changeTo);
					System.out.println("Changing: " + targetColor + 
							" to " + changeTo);
				}
			}
		}
		try {
			boolean temp = ImageIO.write(img, imgType,new File("test." + imgType));
		} catch (IOException e) {
			System.out.println("Failed to write the image");
			e.printStackTrace();
		}	
		System.out.println(imgType);
	}
	
	
	/**
	 * Changes all the pixels within the image to a specific value
	 */
	public void changeAll () {
		for (int i = 0; i < img.getHeight(); i++) {
			for (int z = 0; z < img.getWidth(); z++) {
				img.setRGB(z, i, 0xFFFFFFFF);
				System.out.println("Stuck here " + z + " " + i);
			}	
		}
		try {
			boolean temp = ImageIO.write(img, "jpg", new File("test.jpg"));
		} catch (IOException e) {
			System.out.println("Failed to write the image");
			e.printStackTrace();
		}	
	}
	
	/**
	 * Returns the length of the queue containing the frequency
	 * of each pixel- descending order
	 * @return queue size
	 */
	public int getQueueLength () {
		return freqPixel.size();
	}
	
	/**
	 * Display the pixels values the image file
	 * Each line displays the R: red component
	 * G: green component and B: blue component
	 * At the end the number of values iterated over
	 * is printed/ number of pixels
	 */
	public void displayPixels () {
		for (int i = 0; i < img.getHeight(); i++) {
			for (int z = 0; z < img.getWidth(); z++) {
				Color temp = new Color(img.getRGB(z, i));
				System.out.println("R:" + temp.getRed() + " G:" + temp.getGreen() + " B:" + temp.getBlue());
			}
		}
		System.out.println(img.getHeight()*img.getWidth());
	}
	
	public static void main (String args[]) {
		assert(args.length > 0);
		// Takes argument and checks if the path
		// leads to a valid file
		File fImage = new File(args[0]);
		if (!fImage.exists()) {
			System.out.println("System failed");
			System.exit(1);
		}

		// Creates new image processing object and evaluates
		// the presence of individual colors
		ImageProcessor iP = new ImageProcessor(new File (args[0]));
		iP.countPixels();
		s = new Scanner(System.in);
		
		// Scans for data from stdin
		while (s.hasNextLine()) {
			String nxtLine = s.nextLine();

			if (nxtLine != null & nxtLine.length() != 0) {
				for (String i : nxtLine.split(" ")) {
					try { 
						Color col = Color.decode(i);
						if (iP.getQueueLength() != 0) {
							iP.changeColor(-1, col.getRGB());
						}
					} catch (NumberFormatException e) {
						continue;
					}					
				}
			}
		}
	}		
}
