import java.util.Comparator;


public class PixelCountComparator implements Comparator {

	/** 
	 * Used to compare pixels 
	 * according to their frequency
	 */
	@Override
	public int compare(Object arg0, Object arg1) {
		PixelCount a = (PixelCount) arg0;
		PixelCount b = (PixelCount) arg1;
	
		return b.getFrequency() -a.getFrequency();
	}

}
